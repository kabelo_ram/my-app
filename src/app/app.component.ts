import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {TransactionService} from './transactionService'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ TransactionService ],
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  transactions: any[] = [];

  constructor(private transactionService: TransactionService) { }

editTransaction(transaction: any){
    transaction.editable = !transaction.editable;
  }

  bulkUpdate(transactions: any){
      this.transactionService.bulkUpdate(transactions);
    }

  ngOnInit() {
    this.transactionService.getList().then(data => {
      this.transactions = data;
    });
  }
}
