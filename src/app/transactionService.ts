import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient) { }

  private static TRANSACTION_URL = 'http://localhost:8080/transactions';

  async getList() {
    try {
      const data: any = await this.http.get(TransactionService.TRANSACTION_URL).toPromise();
      return data;
    } catch (error) {
      console.error(`Error occurred: ${error}`);
    }
  }

   async bulkUpdate(transactions: any) {
      try {
            const data: any = this.http.put(TransactionService.TRANSACTION_URL, transactions).subscribe(data => {
            console.log(data);
          });
      } catch (error) {
        console.error(`Error occurred: ${error}`);
      }
    }
}
