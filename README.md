

## required tools

Install Node (https://nodejs.org/en/)

Install Angular CLI:  rn the following command in the terminal: npm install -g @angular/cli

Clone the repo from bitbucket: git clone https://kabelo_ram@bitbucket.org/kabelo_ram/my-app.git

Run the application from the terminal : cd my-app ; ng serve --open

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## How to use the application

1. click on the edit button for the record that you want to edit.
2. edit the row.
3. Click the edit button again when you are done.
4. Edit other rows if you would like to, using the edit button as instructed.
5. Scroll to the bottom of the page and click the update button in order to persist your changes.
